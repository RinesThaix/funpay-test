<?php

namespace FpDbTest\Interpolation;

class DatabaseAutoInterpolationType extends DatabaseInterpolationType
{

    public function __construct()
    {
        parent::__construct(null);
    }

    public function interpolate(mixed $argument): string
    {
        $type = gettype($argument);
        switch ($type) {
            case 'integer':
            case 'double':
                return strval($argument);
            case 'boolean':
                return strval(intval($argument));
            case 'NULL':
                return 'NULL';
            case 'string':
                return "'$argument'";
            default:
                break;
        }
        throw new \Exception("expected int/float/bool/null/string argument to be passed, but received $argument of type $type");
    }
}
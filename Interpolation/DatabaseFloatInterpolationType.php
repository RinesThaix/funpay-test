<?php

namespace FpDbTest\Interpolation;

class DatabaseFloatInterpolationType extends DatabaseInterpolationType
{

    public function __construct()
    {
        parent::__construct('f');
    }

    public function interpolate(mixed $argument): string
    {
        $type = gettype($argument);
        switch ($type) {
            case 'integer':
            case 'double':
                return strval($argument);
            case 'boolean':
                return strval(intval($argument));
            case 'NULL':
                return 'NULL';
            case 'string':
                $result = strval(floatval($argument));
                /*
                 * The following condition may fail in cases when literal string has been given and
                 * precision problems have been faced. Assuming it's ok to leave it as it is.
                 */
                if ($result === $argument) {
                    return $result;
                }
                break;
            default:
                break;
        }
        throw new \Exception("expected float-like argument to be passed, but received $argument of type $type");
    }
}
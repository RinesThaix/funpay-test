<?php

namespace FpDbTest\Interpolation;

abstract class DatabaseInterpolationType
{

    private static DatabaseInterpolationType $autoInterpolationType;
    /**
     * @var array<DatabaseInterpolationType>
     */
    private static array $interpolationTypes = [];

    private static function getInterpolationTypes(): array {
        if (!self::$interpolationTypes) {
            self::$autoInterpolationType = new DatabaseAutoInterpolationType();
            /**
             * @var $interpolationTypes array<DatabaseInterpolationType>
             */
            $identifier_type = new DatabaseIdentifierInterpolationType();
            $interpolationTypes = [
                new DatabaseIntegerInterpolationType(),
                new DatabaseFloatInterpolationType(),
                new DatabaseArrayInterpolationType(self::$autoInterpolationType, $identifier_type),
                $identifier_type
            ];
            foreach ($interpolationTypes as $interpolationType) {
                self::$interpolationTypes[$interpolationType->getTriggeringCharacter()] = $interpolationType;
            }
        }
        return self::$interpolationTypes;
    }

    public static function getInterpolationType(?string $triggering_char): ?DatabaseInterpolationType {
        $interpolation_types = self::getInterpolationTypes();
        if ($triggering_char === null) {
            return self::$autoInterpolationType;
        }
        return $interpolation_types[$triggering_char] ?? null;
    }

    private ?string $triggering_character;

    protected function __construct($triggering_character) {
        $this->triggering_character = $triggering_character;
    }

    public function getTriggeringCharacter(): ?string {
        return $this->triggering_character;
    }

    public abstract function interpolate(mixed $argument): string;

}
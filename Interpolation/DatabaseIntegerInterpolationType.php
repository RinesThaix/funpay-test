<?php

namespace FpDbTest\Interpolation;

class DatabaseIntegerInterpolationType extends DatabaseInterpolationType
{

    public function __construct()
    {
        parent::__construct('d');
    }

    public function interpolate(mixed $argument): string
    {
        $type = gettype($argument);
        switch ($type) {
            case 'integer':
                return strval($argument);
            case 'double':
            case 'boolean':
                return strval(intval($argument));
            case 'NULL':
                return 'NULL';
            case 'string':
                $result = strval(intval($argument));
                if ($result === $argument) {
                    return $result;
                }
                break;
            default:
                break;
        }
        throw new \Exception("expected integer-like argument to be passed, but received $argument of type $type");
    }
}
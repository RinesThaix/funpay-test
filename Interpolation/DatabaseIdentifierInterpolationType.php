<?php

namespace FpDbTest\Interpolation;

class DatabaseIdentifierInterpolationType extends DatabaseInterpolationType
{

    public function __construct()
    {
        parent::__construct('#');
    }

    public function interpolate(mixed $argument): string
    {
        $type = gettype($argument);
        switch ($type) {
            case 'integer':
            case 'double':
            case 'boolean':
            case 'string':
                return self::escape($argument);
            case 'array':
                return implode(
                    ', ',
                    array_map(
                        function (mixed $item): string {
                            return self::escape($item);
                        },
                        $argument
                    )
                );
            default:
                break;
        }
        throw new \Exception("expected identifier-like argument to be passed, but received $argument of type $type");
    }

    private static function escape(mixed $value): string
    {
        return "`$value`";
    }
}
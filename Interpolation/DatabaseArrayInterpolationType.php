<?php

namespace FpDbTest\Interpolation;

class DatabaseArrayInterpolationType extends DatabaseInterpolationType
{

    private DatabaseInterpolationType $autoInterpolationType;
    private DatabaseInterpolationType $identifierInterpolationType;

    public function __construct(
        DatabaseAutoInterpolationType $autoInterpolationType,
        DatabaseIdentifierInterpolationType $identifierInterpolationType
    )
    {
        parent::__construct('a');
        $this->autoInterpolationType = $autoInterpolationType;
        $this->identifierInterpolationType = $identifierInterpolationType;
    }

    public function interpolate(mixed $argument): string
    {
        $type = gettype($argument);
        switch ($type) {
            case 'array':
                if (array_is_list($argument)) {
                    return implode(
                        ', ',
                        array_map(
                            function (mixed $value) {
                                return $this->autoInterpolationType->interpolate($value);
                            },
                            $argument
                        )
                    );
                } else {
                    return implode(
                        ', ',
                        self::array_map_assoc(
                            function (string $key, mixed $value): string {
                                $k = $this->identifierInterpolationType->interpolate($key);
                                $v = $this->autoInterpolationType->interpolate($value);
                                return "$k = $v";
                            },
                            $argument
                        )
                    );
                }
            default:
                break;
        }
        throw new \Exception("expected array argument to be passed, but received $argument of type $type");
    }

    private static function array_map_assoc(callable $callback, array $array): array {
        $result = [];
        foreach ($array as $key => $value) {
            $result[] = $callback($key, $value);
        }
        return $result;
    }
}
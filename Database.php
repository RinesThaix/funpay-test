<?php

namespace FpDbTest;

use Exception;
use FpDbTest\Interpolation\DatabaseInterpolationType;

class Database implements DatabaseInterface
{

    public function buildQuery(string $query, array $args = []): string
    {
        $result = '';
        $query_chars = mb_str_split($query);
        $query_length = count($query_chars);
        $arg_index = 0;
        $condition_block = new DatabaseConditionBlock();
        for ($char_index = 0; $char_index < $query_length; $char_index++) {
            $current_char = $query_chars[$char_index];
            if ($current_char === '?') {
                $interpolation_type = null;
                if ($char_index < $query_length - 1) {
                    $next_char = $query_chars[$char_index + 1];
                    $interpolation_type = DatabaseInterpolationType::getInterpolationType($next_char);
                    if ($interpolation_type !== null) {
                        $char_index++;
                    }
                }
                if ($interpolation_type === null) {
                    $interpolation_type = DatabaseInterpolationType::getInterpolationType(null);
                }
                $argument = $args[$arg_index++] ?? throw new Exception("not enough arguments passed");
                if ($argument === $this->skip()) {
                    if (!$condition_block->isActive()) {
                        throw new Exception("skipping is allowed for arguments within condition blocks only");
                    }
                    $condition_block->fail();
                } else {
                    $interpolated_argument = $interpolation_type->interpolate($argument);
                    if ($condition_block->isActive()) {
                        $condition_block->append($interpolated_argument);
                    } else {
                        $result .= $interpolated_argument;
                    }
                }
            } elseif ($current_char === '{') {
                if ($condition_block->isActive()) {
                    throw new Exception('nested condition blocks are not allowed');
                }
                $condition_block->activate();
            } elseif ($current_char === '}') {
                if (!$condition_block->isActive()) {
                    throw new Exception("parsed condition block end '}', but there was not his start '{");
                }
                if (!$condition_block->isFailed()) {
                    $result .= $condition_block->getData();
                }
                $condition_block->deactivate();
            } else {
                if ($condition_block->isActive()) {
                    $condition_block->append($current_char);
                } else {
                    $result .= $current_char;
                }
            }
        }
        return $result;
    }

    public function skip(): DatabaseSkipValue
    {
        return DatabaseSkipValue::getInstance();
    }

}

<?php

namespace FpDbTest;

class DatabaseConditionBlock
{

    private bool $active = false;
    private bool $failed = false;
    private string $data = '';

    public function activate(): void {
        $this->active = true;
        $this->failed = false;
        $this->data = '';
    }

    public function deactivate(): void {
        $this->active = false;
    }

    public function isActive(): bool {
        return $this->active;
    }

    public function fail(): void {
        $this->failed = true;
    }

    public function isFailed(): bool {
        return $this->failed;
    }

    public function append(string $data): void {
        $this->data .= $data;
    }

    public function getData(): string {
        return $this->data;
    }

}